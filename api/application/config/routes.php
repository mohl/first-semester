<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'BaseController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/**
 * Topics
 */

$route['topics']['GET']             = 'TopicController/index';
$route['topics']['POST']            = 'TopicController/store';
$route['topics/(:any)']['GET']      = 'TopicController/show/$1';
$route['topics/(:any)/edit']['GET'] = 'TopicController/edit/$1';
$route['topics/(:any)']['PUT']      = 'TopicController/update/$1';
$route['topics/(:any)']['DELETE']   = 'TopicController/destroy/$1';

/**
 * Quizzes
 */
$route['quizzes']['GET']              = 'QuizController/index';
$route['quizzes']['POST']             = 'QuizController/store';

$route['quizzes/(:any)']['GET']       = 'QuizController/show/$1';

$route['quizzes/(:any)/edit']['GET']  = 'QuizController/edit/$1';
$route['quizzes/(:any)']['PUT']       = 'QuizController/update/$1';
$route['quizzes/(:num)']['DELETE']    = 'QuizController/destroy/$1';
$route['quizzes/submit']['POST']      = 'QuizController/submit';

/**
 * Questions
 */
$route['questions/quiz/(:num)']['GET'] = 'QuestionController/index/$1';
$route['questions']['POST']            = 'QuestionController/store';
$route['questions/(:num)/edit']['GET'] = 'QuestionController/edit/$1';
$route['questions/(:any)']['GET']      = 'QuestionController/show/$1';
$route['questions/(:num)']['PUT']      = 'QuestionController/update/$1';
$route['questions/(:num)']['DELETE']   = 'QuestionController/destroy/$1';

/**
 * Answers
 */
$route['answers/question/(:num)']['GET'] = 'AnswerController/index/$1';
$route['answers']['POST']                = 'AnswerController/store';
$route['answers/(:num)/edit']['GET']     = 'AnswerController/edit/$1';
$route['answers/(:num)']['PUT']          = 'AnswerController/update/$1';
$route['answers/(:num)']['DELETE']       = 'AnswerController/destroy/$1';
$route['answers/check']['POST']          = 'AnswerController/check';

/**
 * Authentication
 */
$route['auth']['GET']                  = 'auth/index';
$route['auth']['POST']                 = 'AuthController/login';
$route['auth/logout']['GET']           = 'AuthController/logout';
$route['register']['POST']             = 'AuthController/register';
/**
 * Tools
 */
$route['tools/migrate']                = 'tools/migrate/index';
$route['tools/migrate/make/(:any)']    = 'tools/migrate/make/$1';
$route['tools/migrate/version/(:num)'] = 'tools/migrate/version/$1';

$route['tools/seed']                   = 'tools/seed/index';
$route['tools/seed/make/(:any)']       = 'tools/seed/make/$1';


$route['(.*)']                         = 'BaseController/index/$1';