<?php

$base = [
	'quiz' => [
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|max_length[45]'
		],
		[
			'field' => 'slug',
			'label' => 'Slug',
			'rules' => 'trim|required|max_length[45]|validate_unique[quizzes.slug]'
		],
		[
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim|required'
		],
		[
			'field' => 'experience',
			'label' => 'Experience',
			'rules' => 'trim|required|integer|max_length[5]'
		],
		[
			'field' => 'sticky',
			'label' => 'Sticky',
			'rules' => 'trim|required|integer|max_length[1]|in_list[0,1]'
		],
		[
			'field' => 'topic_id',
			'label' => 'Topic ID',
			'rules' => 'trim|required|integer|max_length[5]|validate_topic_id'
		],
		[
			'field' => 'published_at',
			'label' => 'Published at',
			'rules' => 'trim|required|regex_match[/^(\d{4})-(\d{2})-(\d{2}) (\d{2}:\d{2}:\d{2})$/]'
		],
		[
			'field' => 'media_url',
			'label' => 'Media URL',
			'rules' => 'trim|required'
		],
	],
	'question' => [
		[
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'trim|required'
		],
		[
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim'
		],
		[
			'field' => 'explanation',
			'label' => 'Explanation',
			'rules' => 'trim|required|min_length[10]'
		],
		[
			'field' => 'position',
			'label' => 'Position',
			'rules' => 'trim|required|integer'
		],
		[
			'field' => 'quiz_id',
			'label' => 'Quiz ID',
			'rules' => 'trim|required|integer|validate_quiz_id'
		],
		[
			'field' => 'media_type',
			'label' => 'Media type',
			'rules' => 'trim|in_list[0,1]'
		],
		[
			'field' => 'media_url',
			'label' => 'Media URL',
			'rules' => 'trim'
		],
		[
			'field' => 'media_id',
			'label' => 'Media ID',
			'rules' => 'trim|integer'
		],
	],
	'user' => [
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|min_length[3]'
		],
		[
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required'
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required|min_length[9]'
		]
	]
];

$config = [
	'quiz' => $base['quiz'],
	'quiz_update' => array_merge($base['quiz'], [
		[
			'field' => 'id',
			'label' => 'ID',
			'rules' => 'trim|required|integer|validate_quiz_id'
		],
		[
			'field' => 'media_id',
			'label' => 'Media ID',
			'rules' => 'trim|required|integer',
		]
	]),
	'question' => $base['question'],
	'question_update' => array_merge($base['question'], [
		[
			'field' => 'id',
			'label' => 'ID',
			'rules' => 'trim|required|integer|validate_question_id'
		]
	]),
	'answer' => [
		[
			'field' => 'answer',
			'label' => 'Answer',
			'rules' => 'trim|required|min_length[2]'
		],
		[
			'field' => 'correct',
			'label' => 'Correct',
			'rules' => 'trim|required|integer|in_list[0,1]'
		],
		[
			'field' => 'position',
			'label' => 'Position',
			'rules' => 'trim|required|integer'
		],
		[
			'field' => 'question_id',
			'label' => 'Questin ID',
			'rules' => 'trim|required|integer|validate_question_id'
		],
		[
			'field' => 'media_type',
			'label' => 'Media type',
			'rules' => 'trim|integer|in_list[0,1]'
		],
		[
			'field' => 'media_url',
			'label' => 'Media URL',
			'rules' => 'trim'
		],
		[
			'field' => 'media_id',
			'label' => 'Media ID',
			'rules' => 'trim|integer'
		],
	],
	'answer_update' => [
		[
			'field' => 'id',
			'label' => 'ID',
			'rules' => 'trim|required|integer'
		],
		[
			'field' => 'answer',
			'label' => 'Answer',
			'rules' => 'trim|required|min_length[2]'
		],
		[
			'field' => 'correct',
			'label' => 'Correct',
			'rules' => 'trim|required|integer|in_list[0,1]'
		],
		[
			'field' => 'position',
			'label' => 'Position',
			'rules' => 'trim|required|integer'
		],
		[
			'field' => 'question_id',
			'label' => 'Questin ID',
			'rules' => 'trim|required|integer|validate_question_id'
		],
		[
			'field' => 'media_type',
			'label' => 'Media type',
			'rules' => 'trim|integer|in_list[0,1]'
		],
		[
			'field' => 'media_url',
			'label' => 'Media URL',
			'rules' => 'trim'
		],
		[
			'field' => 'media_id',
			'label' => 'Media ID',
			'rules' => 'trim|integer'
		],
	],
	'answer_validate' => [
		[
			'field' => 'answer_id',
			'label' => 'Answer ID',
			'rules' => 'trim|required|integer|validate_question_id'
		],
		[
			'field' => 'question_id',
			'label' => 'Question ID',
			'rules' => 'trim|required|integer|validate_question_id'
		]
	],
	'quiz_submit' => [
		[
			'field' => 'id',
			'label' => 'ID',
			'rules' => 'trim|required|integer|validate_quiz_id'
		],
	],
	'user' => $base['user']
];

/*
$config = array(

	'topic' => array(
		array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|max_length[45]',
			
		),
		array(
			'field' => 'slug',
			'label' => 'Slug',
			'rules' => 'trim|required|max_length[45]'
			
		),
		array(
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim|required'
		)
	),

	'quiz' => array(
		array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[45]'

		),
		array(
			'field' => 'slug',
			'label' => 'Slug',
			'rules' => 'required|max_length[45]'
		),
		array(
			'field' => 'experience',
			'label' => 'Experience',
			'rules' => 'required|max_length[4]'
		),
		array(
			'field' => 'topic_id',
			'label' => 'Topic',
			'rules' => 'required|max_length[11]'
		),
		array(
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'required|max_length[255]'
		),

		'question' => array(
		array(
			'field' => 'tittle',
			'label' => 'Tittle',
			'rules' => 'required|max_length[45]'
		),

		array(
			'field' => 'question',
			'label' => 'Question',
			'rules' => 'required|max_length[255]'
		),

		array(
			'field' => 'position',
			'label' => 'Position',
			'rules' => 'required|max_length[2]'
		),

		array(
			'field' => 'type',
			'label' => 'Type',
			'rules' => 'required|max_length[1]'
		),

		array(
			'field' => 'quiz_id',
			'label' => 'Quiz_id',
			'rules' => 'required|max_length[11]'
		),

		'answer' => array(
		array(
			'field' => 'answer',
			'label' => 'Answer',
			'rules' => 'required|max_length[255]'

		),
		
		array(
			'field' => 'sequence',
			'label' => 'Sequence',
			'rules' => 'required|max_length[2]'
		),

		array(
			'field' => 'correct',
			'label' => 'Correct',
			'rules' => 'required|max_length[1]'
		),

		array(
			'field' => 'type',
			'label' => 'Type',
			'rules' => 'required|max_length[1]'
		),

		array(
			'field' => 'question_id',
			'label' => 'Question_id',
			'rules' => 'required|max_length[11]'
		),

		array(
			'field' => 'media_id',
			'label' => 'Media_id',
			'rules' => 'required|max_length[11]'
		),

		'class' => array(
		array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[45]'
		),

		 	'media' => array(
		array(
			'field' => 'type',
			'label' => 'Type',
			'rules' => 'required|max_length[1]'

		),

		array(
			'field' => 'url',
			'label' => 'Url',
			'rules' => 'required|max_length[255]'
		),

		'score' => array(
		array(
			'field' => 'score',
			'label' => 'Score',
			'rules' => 'required|max_length[11]'

		),
		array(
			'field' => 'user_id',
			'label' => 'User_id',
			'rules' => 'required|max_length[11]'

		),
		array(
			'field' => 'quiz_id',
			'label' => 'Quiz_id',
			'rules' => 'required|max_length[11]'

		),
		'user' => array(
		array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|max_length[45]'

		),
		array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|max_length[255]'

		),
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required|max_length[60]'

		),
		array(
			'field' => 'type',
			'label' => 'Type',
			'rules' => 'required|max_length[1]'

		),
		array(
			'field' => 'api_token',
			'label' => 'Api_token',
			'rules' => 'required|max_length[60]'

		),
	)

);*/
