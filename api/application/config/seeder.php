<?php

/*
|--------------------------------------------------------------------------
| Enable/disable seeds
|--------------------------------------------------------------------------
|
| Seeding is disabled by default but can easily be enabled.
|
*/
$config['seed_enabled'] = TRUE;

/*
|--------------------------------------------------------------------------
| Seeds path
|--------------------------------------------------------------------------
|
| Path to your migrations folder.
| Typically, it will be within your application path.
| Also, writing permission is required within the migrations path.
|
*/
$config['seed_directory'] = APPPATH . 'database/seeds/';

/*
|--------------------------------------------------------------------------
| Template path
|--------------------------------------------------------------------------
| 
|
*/
$config['template_directory'] = APPPATH . 'templates/seed.txt';