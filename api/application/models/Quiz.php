<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends CI_Model
{
	
	private $table = 'quizzes';

	/**
	 * Columns for public view
	 * @var string
	 */
	private $publicColumns = 'id, name, slug, description, experience, topic_id';

	/**
	 * Columns for private view
	 * @var string
	 */
	private $privateColumns = 'id, name, description, created_at, updated_at, sticky';

    /**
     * Create a new model instance.
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
	}

	public function insert(array $data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function getList()
	{
		return $this->db
			->select('q.name, q.slug, q.description, q.experience, q.created_at, q.sticky, q.topic_id')
			->select('t.name AS topic_name')
			->select('m.media_type, m.media_url, m.media_start, m.media_end')
			->select('COUNT(qu.id) as questions')
			->from($this->table . ' q')
			->join('media m', 'q.media_id = m.id')
			->join('topics t', 'q.topic_id = t.id')
			->join('questions qu', 'qu.quiz_id = q.id', 'left')
			->where('q.published_at <', date('Y-m-d H:i:s'))
			->group_by('slug')
			->order_by('created_at')
			->get()
			->result();
	}

	public function getBySlug(string $slug)
	{
		return $this->db
			->select('
				t.name AS topic,
				q.name, 
				q.slug, 
				q.description, 
				q.experience,
				q.created_at,
				m.media_url,
				COUNT(qu.id) as questions')
			->from('quizzes q')
			->join('media m', 'q.media_id = m.id')
			->join('topics t', 'q.topic_id = t.id')
			->join('questions qu', 'qu.quiz_id = q.id')
			->where('q.slug', $slug)
			->where('q.slug is not null')
			->get()
			->row();
	}

	public function whereSlugIs(string $slug)
	{
		$test = 'm.type as media_type, ';

		return $this->db
			->select('q.*, m.media_url')
			->from('quizzes q')
			->join('media m', 'q.media_id = m.id')
			->where('slug', $slug)
			->get()
			->row();
	}

	public function updateWhereSlugIs(string $slug, array $data)
	{
		return $this->db
			->where('slug', $slug)
			->update($this->table, $data);
	}

	public function deleteWhereIdIs(int $id)
	{
		return $this->db
			->where('id', $id)
			->delete($this->table);
	}

	public function validateByID($id)
	{
		return $this->db
			->select('name')
			->from('quizzes')
			->where('id', $id)
			->get()
			->row();
	}

	public function validateBySlug(string $slug)
	{
		return $this->db
			->select('name')
			->from('quizzes')
			->where('slug', $slug)
			->get()
			->row();
	}

	public function hasBeenAnswered(int $qid, int $uid)
	{

		$data1 = [];

		$questionIDs = $this->db
			->select('id')
			->from('questions q')
			->where('quiz_id', $qid)
			->get()
			->result();

		foreach($questionIDs as $key) {
			$data1[] = $key->id;
		}

		$data2 = [];

		$answeredIDs = $this->db
			->select('ua.question_id')
			->from('users_answers ua')
			->join('questions q', 'ua.question_id = q.id', 'right')
			->where('ua.question_id IS NOT NULL')
			->where('q.quiz_id', $qid)
			->get()
			->result();

		foreach($answeredIDs as $key) {
			$data2[] = $key->question_id;
		}

		return ($data1 == $data2);

	}

	public function addCompletion(array $data)
	{
		return $this->db->insert('users_quizzes', $data);
	}

	public function getMediaIds(int $id)
	{

		$array = [];

		$result = $this->db->query('select media_id 
				from quizzes
				where id=' . $id . ' AND media_id IS NOT NULL
				UNION
				select media_id
				from questions
				where quiz_id='. $id .' AND media_id IS NOT NULL
				UNION
				select answers.media_id
				from answers,questions
				where answers.question_id=questions.id and questions.quiz_id = '. $id .' and answers.media_id IS NOT NULL
				order by media_id');

		foreach ( $result->result() as $row ) {
			$array[] = $row->media_id;
		}

		return $array;
	}
	
	public function hasBeenCompleted(int $uid, int $qid)
	{
		return $this->db
			->where(['user_id' => $uid, 'quiz_id' => $qid])
			->count_all_results('users_quizzes');
	}

}