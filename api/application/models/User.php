<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model
{
	
	private $table = 'users';

    /**
     * Create a new model instance.
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
	}

	public function whereEmailIs(string $email)
	{
		return $this->db
				->select('id, password')
				->from($this->table)
				->where('email', $email)
				->get()
				->row();
	}
	
	public function whereRoleIs(int $role)
	{
		return $this->db
			->where('role >=', $role)
			->get($this->table)
			->row();
	}

	public function register(array $data)
	{
		return $this->db
			->insert('users', $data);
	}
	
}