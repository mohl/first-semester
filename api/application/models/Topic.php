<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topic extends CI_Model
{

	/**
	 * Topics table
	 * @var string
	 */
	private $table         = 'topics';

	/**
	 * Columns for public view
	 * @var string
	 */
	private $publicColumns = 'id, name, slug, description';

	/**
	 * Columns for private view
	 * @var string
	 */
	private $privateColumns = 'id, name, description, created_at, updated_at';
 
	
    /**
     * Create a new model instance.
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
	}

	public function getList()
	{
		return $this->db
			->select($this->publicColumns)
			->from($this->table)
			->order_by('name ASC')
			->get()
			->result();
	}

	public function insert($data)
	{
		$this->db->insert($this->table, $data);
		return [
			'id'   => $this->db->insert_id(),
			'name' => $data['name']
		];
	}

	public function updateWhereSlugIs(string $slug, array $data)
	{
		return $this->db
			->where('slug', $slug)
			->update($this->table, $data);
	}

	public function deleteWhereSlugIs(string $slug)
	{
		return $this->db
			->where('slug', $slug)
			->delete($this->table);
	}

	public function getWhereSlugIs(string $slug, bool $private = false)
	{
		return $this->db
			->select($private ? $this->privateColumns : $this->publicColumns)
			->from($this->table)
			->where('slug', $slug)
			->get()
			->row();
	}

	public function getIdBySlug(string $slug)
	{
		return $this->db
			->select('id')
			->where('slug', $slug)
			->get($slug)
			->row();
	}
	
	public function validateByID($id)
	{
		return $this->db
			->select('name')
			->from('topics')
			->where('id', $id)
			->get()
			->row();
	}

	public function getQuizzesBySlug(string $slug)
	{

		return $this->db
			->select('t.name, t.slug, q.name, q.slug')
			->from('topics t')
			->join('quizzes q', 'q.topic_id = t.id')
			->where('t.slug', $slug)
			->get()
			->result();

	}

}