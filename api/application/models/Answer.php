<?php

class Answer extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	
	public function listByQuestionId(int $id)
	{
		return $this->db
			->select('id, answer, position, correct, created_at, updated_at')
			->from('answers')
			->where('question_id', $id)
			->order_by('position ASC')
			->get()
			->result();
	}

	public function insert($data)
	{
		$this->db->insert('answers', $data);
		return $this->db->insert_id();
	}

	public function whereQuestionIdIs(int $id)
	{
		$query = $this->db->query('SELECT a.*, m.media_type, m.media_url, m.media_start, m.media_end FROM answers a LEFT JOIN media m ON a.media_id = m.id WHERE a.question_id = ' . $id . ' ORDER BY a.position ASC, a.created_at ASC');

		$row = $query->result();
		return $row;
	}
	
	public function whereIdIs(int $id)
	{
		return $this->db
			->select('a.*, m.media_url, m.media_type, m.media_start, m.media_end, m.media_url')
			->from('answers a')
			->join('media m', 'a.media_id = m.id', 'left')
			->where('a.id', $id)
			->get()
			->row();
	}
	
	public function getExplanation(int $qid)
	{
		return $this->db
			->select('explanation')
			->from('questions')
			->where('id', $qid)
			->get()
			->row();
	}

	public function getForQuiz(int $id)
	{
		return $this->db
			->select('a.id, a.answer')
			->select('m.media_type, m.media_url, m.media_start, m.media_end')
			->from('answers a')
			->join('media m', 'a.media_id = m.id', 'left')
			->where('a.question_id', $id)
			->order_by('position ASC', 'created_at ASC')
			->get()
			->result();
	}

	public function updateWhereIdIs(int $id, array $data)
	{
		return $this->db
			->where('id', $id)
			->update('answers', $data);
	}

	public function validateId(int $id)
	{
		return $this->db->select('id')->where('id', $id)->get('answers')->row();
	}

	public function deleteWhereIdIs(int $id)
	{
		$this->db
			->where('id', $id)
			->delete('answers');

		return $this->db->affected_rows() ? TRUE : FALSE;		
	}

	public function validateByID($id)
	{
		return $this->db
			->select('answer')
			->from('answers')
			->where('id', $id)
			->get()
			->row();
	}

	public function isCorrect(int $qid, int $aid)
	{
		return $this->db->where(['id' => $aid, 'question_id' => $qid, 'correct' => 1])
			->count_all_results('answers');

	}

	public function addUserAnswer(array $data)
	{
		return $this->db
			->insert('users_answers', $data);
	}

	public function isAnswered(int $uid, int $aid)
	{
		return $this->db
			->where(['user_id' => $uid, 'answer_id' => $aid])
			->count_all_results('users_answers');
	}

}
