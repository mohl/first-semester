<?php

class Question extends CI_Model
{

	public function insert($data)
	{
		$this->db->insert('questions', $data);

		return [
			'id'   => $this->db->insert_id(),
			'name' => $data['title']
		];
	}

	public function listByQuizId(int $id)
	{
		return $this->db
			->select('q.id, q.title, q.created_at, q.updated_at')
			->from('questions q')
			#->join('media m', 'q.media_id = m.id')
			->where('q.quiz_id', $id)
			->get()
			->result();
	}

	public function getByQuizId(int $id)
	{
		return $this->db
			->select('q.title, q.id, m.media_type, m.media_url, m.media_start, m.media_end')
			->from('questions q')
			->join('media m', 'q.media_id = m.id')
			->where('q.quiz_id', $id)
			->get()
			->row();
	}

	public function whereIdIs(int $id)
	{
		$query = $this->db->query('SELECT q.*, m.media_type, m.media_url, m.media_start, m.media_end FROM questions q LEFT JOIN media m ON q.media_id = m.id WHERE q.id = ' . $id);

		$row = $query->row();
	
		return $row;
	}

	public function updateWhereIdIs(int $id, array $data)
	{
		return $this->db
			->where('id', $id)
			->update('questions', $data);
	}

	public function isUnique(string $title)
	{
		return $this->db
			->where('title', $title)
			->get('questions')
			->row();
	}

	public function getMediaId(int $id)
	{
		return $this->db
			->select('media_id')
			->where('id', $id)
			->get('questions')
			->row();
	}

	public function deleteWhereIdIs(int $id)
	{

		$this->db
			->where('id', $id)
			->delete('questions');

		return $this->db->affected_rows() ? TRUE : FALSE;
		
	}

	public function getMediaIds(int $id)
	{
		$array = [];

		$result = $this->db->query('
			SELECT media_id FROM questions WHERE id = ' . $id . ' 
			AND media_id IS NOT NULL
			UNION
			SELECT media_id FROM answers WHERE question_id = '.$id.' 
			AND media_id IS NOT NULL');

		foreach ( $result->result() as $row ) {
			$array[] = $row->media_id;
		}

		return $array;


	}


	public function validateByID($id)
	{
		return $this->db
			->select('title')
			->from('questions')
			->where('id', $id)
			->get()
			->row();
	}

}