<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Token extends CI_Model
{

	private $table = 'tokens';

	
    /**
     * Create a new model instance.
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
	}

	public function giveUserToken(array $data)
	{
		return $this->db
				->insert($this->table, $data);
	}

	public function revokeUserToken(int $id, string $token)
	{
		return $this->db
				->where('id', $id)
				->where('token', $token)
				->delete($this->table);
	}
	
	public function whereUserIdAndTokenIs(int $id, string $token)
	{
		return $this->db
			->from($this->table)
			->where('user_id', $id)
			->where('token', $token)
			->get()
			->row();
	}

}