<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Model
{
	private $table = 'media';

    /**
     * Create a new model instance.
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
	}

	public function insert(array $data)
	{
		$this->db
			->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function updateWhereIdIs(int $id, array $data)
	{
		return $this->db
			->where('id', $id)
			->update($this->table, $data);
	}

	public function deleteWhereIdIs(int $id)
	{
		$this->db->where('id', $id)->delete($this->table);
	}

	public function deleteBatch(array $data)
	{
		$this->db->where_in('id', $data);
		$this->db->delete('media');
	}
	
	/*public function deleteWhereQuizSlugIs($slug)
	{

	}*/


	
}
