<?php

if( !function_exists('filter_header'))
{
	function filter_header($header)
	{

		$header      = explode(' ', $header);
		$header_data = explode(':', base64_decode($header[1]));

	    // Check if there is no invalid character in string
    	if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $header[1])) {
    		$ci =& get_instance();
			$ci->response->status(400)->json([
				'message' => 'Bad request'
			]);
    	}	
		
		return [
			$header_data[0],
			$header_data[1]
		];

	}
}