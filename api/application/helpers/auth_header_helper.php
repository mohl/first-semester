<?php

if( !function_exists('auth_header'))
{
	function auth_header()
	{

		$ci =& get_instance();

		if( !$header = $ci->input->get_request_header('Authorization', TRUE))
		{
			$ci->response->status(400)->json([
				'message' => 'Bad request'
			]);
		}

		return $header;

	}
}