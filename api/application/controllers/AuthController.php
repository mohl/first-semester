<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller
{
	
    /**
     * Create a knew controller instance
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('token');
		$this->load->helper('auth_header');
		$this->load->helper('filter_header');
	}
	
	public function login()
	{
		$this->load->model('user');
		$this->load->helper('string');

		$header                 = auth_header();
		list($email, $password) = filter_header($header);

		if ( !$user = $this->user->whereEmailIs($email))
		{
			$this->response->json(['message' => 'error', 'errors' => [
				'login' => 'wrong username and/or password'
				]]);
		}

		if ( !password_verify($password, $user->password))
		{
			$this->response->json(['message' => 'error', 'errors' => [
				'login' => 'wrong username and/or password'
				]]);
		}

		$token = random_string('sha1');
		$data = [
			'user_id'    => $user->id,
			'token'      => $token,
			'expires_at' => date('Y-m-d H:i:s', strtotime('+1 hour'))
		];

		$this->token->giveUserToken($data);

		$this->response->json([
			'message' => 'authenticated',
			'info' => [
				'id' => $user->id,
				'token' => $token
			]
		]);
	}

	public function logout()
	{
		$header = auth_header();
		list($id, $token) = filter_header($header);

		$this->token->revokeUserToken($id, $token);

		$this->response->json([
			'message' => 'revoked'
		]);
	}

	public function register()
	{
		
		$this->load->model('user');
		$this->load->library('form_validation');

		if ( $this->form_validation->run('user') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		$data = [
			'name' => FILTER_VAR($this->input->post('name'), FILTER_SANITIZE_STRING),
			'email' => FILTER_VAR($this->input->post('email'), FILTER_SANITIZE_EMAIL),
			'password' => 
				password_hash(
					FILTER_VAR($this->input->post('password'), FILTER_SANITIZE_STRING),
				PASSWORD_BCRYPT)
		];

		if( $this->user->register($data) ) {
			$this->response->json([
				'message' => 'registered'
			]);
		}

	}
	
}