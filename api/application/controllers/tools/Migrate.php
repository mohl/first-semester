<?php

class Migrate extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->input->is_cli_request()
		OR show_404();

		$this->load->library('migration');
	}

	/**
	 * Run migrations
	 */
	public function index()
    {
        
        if( ! $this->migration->latest()) 
        {
            show_error($this->migration->error_string());
        }
        else
        {
            echo 'Migration(s) done' . PHP_EOL;
        }

    }

    /**
     * Revert to version
     */
	public function version($version)
    {
        if( ! $this->migration->version($version))
        {
            echo $this->migration->error_string();
        }
        else
        {
            echo 'Migrated to version ' . $version . PHP_EOL;
        }
    }

	public function make(string $migration)
	{
        if($this->migration->make($migration))
        {
        	echo 'Created migration ' . $migration . PHP_EOL;
        }
	}
}