<?php

class Seed extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('seeder');
	}

	public function index()
	{
		
		if($seeds = $this->seeder->run())
		{
			echo 'Seeded ' . $seeds . ' table(s)' . PHP_EOL;
		}

	}

	/**
	 * 
	 */
	public function make($seed)
	{

		$this->seeder->make($seed);

	}



}