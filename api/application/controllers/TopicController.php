<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TopicController extends CI_Controller
{
	
    /**
     * Create a new controller instance
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('topic');
	}
	
	/**
	 * Entry point to the resource
	 *
	 * @return response
	 */
	public function index()
	{
		$topics = $this->topic->getList();
		$this->response->json($topics);
	}
	
	/**
	 * Store a newly created resource
	 *
	 * @return response
	 */
	public function store()
	{
		//$this->guard->method('POST')->access();
		$this->load->library('form_validation');

		// This should be validated
		// use $this->form_validation->errors() to return the errors as an array
		

		/*if($this->form_validation->run('TopicController') === FALSE)
		{
			$this->response->json(array(
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			));
		}*/
		$data = [
			'name'        => $this->input->post('name'),
			'slug'        => $this->input->post('slug'),
			'description' => $this->input->post('description')
		];

		$this->topic->insert($data);

		$this->response->json([
			'message' => 'created'
		]);

	}
	
	/**
	 * Show a resource
	 * 
	 * @param string $slug
	 * @return response
	 */
	public function show(string $slug)
	{

		$slug = FILTER_VAR($slug, FILTER_SANITIZE_STRING);

		if ( !$quizzes = $this->topic->getQuizzesBySlug($slug))
		{
			$this->response->status(404)->json(['message' => 'not found']);
		}

		$this->response->json([
			'topic' => $slug,
			'quizzes' => $quizzes
		]);
	}
	
	/**
	 * Edit a resource
	 *
	 * @param string $slug
	 * @return response
	 */
	public function edit(string $slug)
	{
		//$this->guard->method('GET')->access(1);

		//$slug = filter_var($slug, FILTER_SANITIZE_STRING);

		if ( !$topic = $this->topic->getWhereSlugIs($slug, true))
		{
			$this->response->status(404)->json(['message' => 'not found']);
		}

		$this->response->json($topic);

	}
	
	/**
	 *	Update a resource
	 *
	 * @param string $slug
	 * @return response
	 */
	public function update(string $slug)
	{
		//$this->guard->method('PUT')->access(1);

		//$slug = filter_var($slug, FILTER_SANITIZE_STRING);

		$this->load->library('form_validation');

		// This should be validated
		$data = [
			'name'        => $this->input->input_stream('name'),
			'slug'        => $this->input->input_stream('slug'),
			'description' => $this->input->input_stream('description')
		];

		if ( !$this->topic->updateWhereSlugIs($slug, $data))
		{
			$this->response->status(400)->json(['message' => 'could not update topic']);
		}

		$this->response->json([
			'message' => 'updated'
		]);

	}
	
	/**
	 * Delete a resource
	 *
	 * @param string $slug
	 * @return response
	 */
	public function destroy(string $slug)
	{
		$this->topic->deleteWhereSlugIs($slug);

		return $this->response->json([
			'message' => 'deleted'
		]);
	}
	
}