<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QuizController extends CI_Controller
{
	
    /**
     * Create a new controller instance
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('quiz');
		$this->load->model('media');
	}
	
	/**
	 * Entry point to the resource
	 *
	 * @return response
	 */
	public function index()
	{
		if ( !$quizzes = $this->quiz->getList())
		{
			$this->response->json(['message' => 'No quizzes found']);
		}

		$this->response->json($quizzes);
	}
	
	/**
	 * Store a newly created resource
	 *
	 * @return response
	 */
	public function store()
	{
		#$this->guard->access(1);
		$this->load->library('form_validation');

		if ( $this->form_validation->run('quiz') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		$mediaId  = $this->media->insert([
			'media_url'   => FILTER_VAR($this->input->post('media_url'), FILTER_SANITIZE_URL)
		]);

		$data = [
			'name'         => FILTER_VAR($this->input->post('name'), FILTER_SANITIZE_STRING),
			'slug'         => FILTER_VAR($this->input->post('slug'), FILTER_SANITIZE_URL),
			'description'  => FILTER_VAR($this->input->post('description'), FILTER_SANITIZE_SPECIAL_CHARS),
			'experience'   => FILTER_VAR($this->input->post('experience'), FILTER_SANITIZE_NUMBER_INT),
			'published_at' => FILTER_VAR($this->input->post('published_at'), FILTER_SANITIZE_STRING),
			'sticky'       => FILTER_VAR($this->input->post('sticky'), FILTER_SANITIZE_NUMBER_INT),
			'topic_id'     => FILTER_VAR($this->input->post('topic_id'), FILTER_SANITIZE_NUMBER_INT),
			'media_id'     => $mediaId
		];

		$id = $this->quiz->insert($data);

		$this->response->json([
			'message' => 'created',
			'data' => [
				'id'    => $id,
				'name' => $data['name'],
				'slug'  => $data['slug']
			]
		]);
	}
	
	/**
	 * Show a resource
	 * 
	 * @param string $slug
	 * @return response
	 */
	public function show(string $slug)
	{

		$slug = FILTER_VAR($slug, FILTER_SANITIZE_STRING);

		if ( !$quiz = $this->quiz->getBySlug($slug) ) {
			$this->response->json(['message' => 'not found']);
		}

		$this->response->json($quiz);
	}
	
	/**
	 * Edit a resource
	 *
	 * @param string $slug
	 * @return response
	 */
	public function edit(string $slug)
	{
		#$this->guard->access(1);
		
		if ( !$quiz = $this->quiz->whereSlugIs($slug))
		{
			$this->response->status(404)->json(['message' => 'not found']);
		}

		$this->response->json($quiz);
	}
	
	/**
	 *	Update a resource
	 *
	 * @param string $slug
	 * @return response
	 */
	public function update(string $slug)
	{

		#$this->guard->access(1);

		$slug = FILTER_VAR($slug, FILTER_SANITIZE_STRING);

		if ( !$this->quiz->validateBySlug($slug) ) {
			$this->response->status(400)->json(['message' => 'bad request']);
		}

		$this->load->library('form_validation');

		$this->form_validation->set_data($this->input->input_stream());
		if ( $this->form_validation->run('quiz_update') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		$this->quiz->updateWhereSlugIs($slug, [
			'name'         => FILTER_VAR($this->input->input_stream('name'), FILTER_SANITIZE_STRING),
			'slug'         => FILTER_VAR($this->input->input_stream('slug'), FILTER_SANITIZE_URL),
			'description'  => FILTER_VAR($this->input->input_stream('description'), FILTER_SANITIZE_SPECIAL_CHARS),
			'experience'   => FILTER_VAR($this->input->input_stream('experience'), FILTER_SANITIZE_NUMBER_INT),
			'published_at' => FILTER_VAR($this->input->input_stream('published_at'), FILTER_SANITIZE_STRING),
			'sticky'       => FILTER_VAR($this->input->input_stream('sticky'), FILTER_SANITIZE_NUMBER_INT),
			'topic_id'     => FILTER_VAR($this->input->input_stream('topic_id'), FILTER_SANITIZE_NUMBER_INT),
		]);

		$this->media->updateWhereIdIs((int) FILTER_VAR($this->input->input_stream('media_id'), FILTER_SANITIZE_NUMBER_INT), [
			'media_url'   => FILTER_VAR($this->input->input_stream('media_url'), FILTER_SANITIZE_URL)
		]);

		$this->response->json([
			'message' => 'updated'
		]);


	}
	
	/**
	 * Delete a resource
	 *
	 * @param string $slug
	 * @return response
	 */
	public function destroy(int $id)
	{

		$id = (int) FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		if ( $media = $this->quiz->getMediaIds($id) ) {
			$this->media->deleteBatch($media);
		}

		$this->quiz->deleteWhereIdIs($id);

		$this->response->json(['message' => 'deleted']);
	}

	public function submit()
	{
		$this->guard->access();
		
		$header = auth_header();
		list($id, $tk) = filter_header($header);

		$this->load->library('form_validation');
		if ( $this->form_validation->run('quiz_submit') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		$qid = FILTER_VAR($this->input->post('id'), FILTER_SANITIZE_NUMBER_INT);
		$uid = FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		$this->isDone($uid, $qid);

		if( !$this->quiz->hasBeenAnswered($qid, $uid) ) {
			$this->response->json([
				'message' => 'error',
				'errors' => [
					'Quiz has not been fully answered'
				]
			]);
		}

		$this->quiz->addCompletion([
			'user_id' => $uid,
			'quiz_id' => $qid
		]);

		$this->response->json([
			'message' => 'quiz compelted'
		]);

	}

	private function isDone(int $uid, int $qid)
	{
		if( $this->quiz->hasBeenCompleted($uid, $qid) ) {
			$this->response->json([
				'message' => 'error',
				'errors' => [
					'the quiz has already been completed'
				]
			]);
		}
	}

}
