<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseController extends CI_Controller
{
	public function index($param = NULL)
	{
	    
	    $this->guard->access();
	    
		if ($param !== NULL)
		{
			$this->response
				->status(404)
				->json([
					'message' => 'not found'
				]);
		}

		$this->response->json([
			'message' => 'This is an API, no?'
		]);
	}	
}