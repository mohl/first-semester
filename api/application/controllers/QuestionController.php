<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QuestionController extends CI_Controller
{
	
    /**
     * Create a new controller instance
     *
     * @return void
     */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('question');
		$this->load->model('media');
	}
	
	/**
	 * Entry point to the resource
	 *
	 * @param  int $id Quiz ID
	 * @return response
	 */
	public function index(int $id)
	{
		$id = FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		if( !$questions = $this->question->listByQuizId($id) ) {
			$this->response->json(['message' => 'no questions found']);
		}

		$this->response->json($questions);
	}
	
	/**
	 * Store a newly created resource
	 *
	 * @return response
	 */
	public function store()
	{
		#$this->guard->access(1);
		$this->load->library('form_validation');

		if ( $this->form_validation->run('question') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		if ( $this->input->post('media_type') !== null AND
			 $this->input->post('media_url')  !== null) {
			$mediaId  = $this->media->insert([
				'media_type'  => 
					FILTER_VAR($this->input->post('media_type'), FILTER_SANITIZE_NUMBER_INT),
				'media_url'   => 
					FILTER_VAR($this->input->post('media_url'), FILTER_SANITIZE_URL),
				'media_start' => 
					$this->input->post('media_start')  ? 
					FILTER_VAR($this->input->post('media_start'),FILTER_SANITIZE_STRING) : 
					NULL,
				'media_end'   => 
					$this->input->post('media_end')  ? 
					FILTER_VAR($this->input->post('media_end'),FILTER_SANITIZE_STRING) : 
					NULL,
			]);
		}

		$data = $this->question->insert([
			'title'       => 
				FILTER_VAR($this->input->post('title'), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
			'description' => 
				FILTER_VAR($this->input->post('description'), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
			'explanation' => 
				FILTER_VAR($this->input->post('explanation'), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
			'quiz_id'     => 
				FILTER_VAR($this->input->post('quiz_id'), FILTER_SANITIZE_NUMBER_INT),
			'media_id'    => 
				isset($mediaId) ? FILTER_VAR($mediaId, FILTER_SANITIZE_NUMBER_INT) : null
		]);

		$this->response->json([
			'message' => 'created',
			'data' => $data
		]);
	}
	
	/**
	 * Show a resource
	 * 
	 * @param  int $id Quiz ID
	 * @return response
	 */
	public function show(int $id)
	{

		$this->load->model('answer');

		$question = $this->question->getByQuizId($id);

		$answers  = $this->answer->getForQuiz($question->id);


		$data = [];

		$this->response->json([
			'question' => $question,
			'answers'  => $answers
		]);
	}
	
	/**
	 * Edit a resource
	 *
	 * @param int $id
	 * @return response
	 */
	public function edit(int $id)
	{
		#$this->guard->access(1);

		$id = FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		if ( !$question = $this->question->whereIdIs($id))
		{
			$this->response->status(404)->json(['message' => 'not found']);
		}

		$this->response->json($question);
	}
	
	/**
	 *	Update a resource
	 *
	 * @param string $slug
	 * @return response
	 */
	public function update(int $id)
	{

		$id = (int) FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		$this->check($id);
		#$this->guard->access(1);
		$this->load->library('form_validation');

		$this->form_validation->set_data($this->input->input_stream());
		if ( $this->form_validation->run('question_update') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		if ( $this->input->input_stream('media_type') !== null AND 
			 $this->input->input_stream('media_url') !== null ) {
			
			// Check if quiz has a media_id
			if ( $this->input->input_stream('media_id') !== null ) {

				// Update if it does
				$this->media->updateWhereIdIs($this->input->input_stream('media_id'), [
					'media_type'  => 
						FILTER_VAR($this->input->input_stream('media_type'), FILTER_SANITIZE_NUMBER_INT),
					'media_url'   => 
						FILTER_VAR($this->input->input_stream('media_url'), FILTER_SANITIZE_URL),
					'media_start' => 
						$this->input->input_stream('media_start')  ? 
						FILTER_VAR($this->input->input_stream('media_start'),FILTER_SANITIZE_STRING) : 
						NULL,
					'media_end'   => 
						$this->input->input_stream('media_end')  ? 
						FILTER_VAR($this->input->input_stream('media_end'),FILTER_SANITIZE_STRING) : 
						NULL,
				]);

				$mediaId = (int) FILTER_VAR($this->input->input_stream('media_id'), FILTER_SANITIZE_NUMBER_INT);

			} else {

				$mediaId  = $this->media->insert([
					'media_type'  => 
						FILTER_VAR($this->input->input_stream('media_type'), FILTER_SANITIZE_NUMBER_INT),
					'media_url'   => 
						FILTER_VAR($this->input->input_stream('media_url'), FILTER_SANITIZE_URL),
					'media_start' => 
						FILTER_VAR($this->input->input_stream('media_start'), FILTER_SANITIZE_STRING),
					'media_end'   => 
						FILTER_VAR($this->input->input_stream('media_end'), FILTER_SANITIZE_STRING)
				]);

			}

		}

		$data = [
			'title'       => 
				FILTER_VAR($this->input->input_stream('title'), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
			'description' => 
				FILTER_VAR($this->input->input_stream('description'), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
			'explanation' => 
				FILTER_VAR($this->input->input_stream('explanation'), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
			'media_id'    => 
				$mediaId ? FILTER_VAR($mediaId, FILTER_SANITIZE_NUMBER_INT) : null
		];

		$this->question->updateWhereIdIs($id, $data);

		$this->response->json([
			'message' => 'updated',
			'data' => [
				'id'    => $id,
				'title' => $data['title']
			]
		]);

	}
	
	/**
	 * Delete a resource
	 *
	 * @param int $id
	 * @return response
	 */
	public function destroy(int $id)
	{

		$id = FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		if ( $media = $this->question->getMediaIds($id) ) {
			$this->media->deleteBatch($media);
		}

		#$media = $this->question->getMediaId($id);

		$this->question->deleteWhereIdIs($id);

		/*if ( $media !== null ) {
			$this->media->deleteWhereIdIs($media->media_id);
		}*/

		$this->response->json(['message' => 'deleted']);

	}

	private function check(int $id)
	{
		if ( !$this->question->validateByID($id)) {
			$this->response->status(404)->json(['message' => 'not found']);
		}
	}

}