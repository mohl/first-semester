<?php

class AnswerController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('answer');
		$this->load->model('media');
	}

	/**
	 * Entry point to the resource
	 *
	 * @param  int $id Question ID
	 * @return response
	 */
	public function index(int $id)
	{
		$id = FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		if( !$answers = $this->answer->listByQuestionId($id) ) {
			$this->response->json(['message' => 'no answers found']);
		}

		$this->response->json($answers);
	}

	public function store()
	{

		$this->load->library('form_validation');

		if ( $this->form_validation->run('answer') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		if ( $this->input->post('media_type') !== null AND
			$this->input->post('media_url')  !== null ) {

			$media = [
				'media_type'  => 
					FILTER_VAR($this->input->post('media_type'), FILTER_SANITIZE_NUMBER_INT),
				'media_url'   => 
					FILTER_VAR($this->input->post('media_url'), FILTER_SANITIZE_URL),
				'media_start' =>
					$this->input->post('media_start')  ? 
					FILTER_VAR($this->input->post('media_start'),FILTER_SANITIZE_STRING) : 
					NULL,
				'media_end'   => 
					$this->input->post('media_end')  ? 
					FILTER_VAR($this->input->post('media_end'),FILTER_SANITIZE_STRING) : 
					NULL
			];

			$mediaId  = $this->media->insert($media);
		}

		$data = [
			'answer' => FILTER_VAR($this->input->post('answer'), FILTER_SANITIZE_STRING),
			'correct' => (int) FILTER_VAR($this->input->post('correct'), FILTER_SANITIZE_NUMBER_INT),
			'position' => (int) FILTER_VAR($this->input->post('position'), FILTER_SANITIZE_NUMBER_INT),
			'question_id' => (int) FILTER_VAR($this->input->post('question_id'), FILTER_SANITIZE_NUMBER_INT),
			'media_id' => isset($mediaId) ? $mediaId : null
		];

		if ( !$id = $this->answer->insert($data)) {
			$this->response->json(['message' => 'error', 'error' => 'could not create answer']);
		}

		$this->response->json([
			'message' => 'created',
			'data' => [
				'id' => $id,
				'answer' =>  $data['answer']
			]
		]);

	}

	/**
	 * Edit a resource
	 *
	 * @param int $id
	 * @return response
	 */
	public function edit(int $id)
	{

		$id = (int) FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		if ( !$answer = $this->answer->whereIdIs($id))
		{
			$this->response->status(404)->json(['message' => 'not found']);
		}

		$this->response->json($answer);
	}

	public function update($id)
	{

		if ( !$this->answer->validateId($id)) {
			$this->response->status(400)->json(['message' => 'bad request']);
		}

		$this->load->library('form_validation');

		$this->form_validation->set_data($this->input->input_stream());
		if ( $this->form_validation->run('answer_update') === FALSE) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		$mediaId = null;

		if ( null !== $this->input->input_stream('media_type') AND 
			 null !== $this->input->input_stream('media_url') ) {
			
			// Check if quiz has a media_id
			if ( null !== $this->input->input_stream('media_id') ) {

				// Update if it does
				$this->media->updateWhereIdIs($this->input->input_stream('media_id'), [
					'media_type'  => 
						FILTER_VAR($this->input->input_stream('media_type'), FILTER_SANITIZE_NUMBER_INT),
					'media_url'   => 
						FILTER_VAR($this->input->input_stream('media_url'), FILTER_SANITIZE_URL),
					'media_start' => $this->input->input_stream('media_start'),
					'media_end'   => $this->input->input_stream('media_end')
				]);

				$mediaId = $this->input->input_stream('media_id');

			} else {

				$mediaId  = $this->media->insert([
					'media_type'  => 
						FILTER_VAR($this->input->input_stream('media_type'), FILTER_SANITIZE_NUMBER_INT),
					'media_url'   => 
						FILTER_VAR($this->input->input_stream('media_url'), FILTER_SANITIZE_URL),
					'media_start' => $this->input->input_stream('media_start'),
					'media_end' => $this->input->input_stream('media_end'),
				]);

			}

		}

		$answerId = (int) FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		$data = [
			'answer' => FILTER_VAR($this->input->input_stream('answer'), FILTER_SANITIZE_STRING),
			'correct' => (int) FILTER_VAR($this->input->input_stream('correct'), FILTER_SANITIZE_NUMBER_INT),
			'position' => (int) FILTER_VAR($this->input->input_stream('position'), FILTER_SANITIZE_NUMBER_INT),
			'question_id' => (int) FILTER_VAR($this->input->input_stream('question_id'), FILTER_SANITIZE_NUMBER_INT),
			'media_id' => $mediaId
		];


		$this->answer->updateWhereIdIs($answerId, $data);
		$this->response->json([
			'message'    => 'updated',
			'data' => [
				'id'     => $answerId,
				'answer' => $data['answer']
			]
		]);

	}

	/**
	 * Destroy a resource
	 * 
	 * @param int $id Answer id
	 * @return response 
	 */
	public function destroy(int $id)
	{

		$id = (int) FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);

		$media = $this->answer->getMediaId('answers', $id);

		if ( $media->media_id ) {
			$this->media->deleteWhereIdIs($media->media_id);
		}

		$this->answer->deleteWhereIdIs($id);

		$this->response->json(['message' => 'deleted']);

	}

	public function check()
	{

		$this->load->helper('auth_header');
		$this->load->helper('filter_header');
		$this->load->library('form_validation');

		$header = auth_header();
		list($id, $tk) = filter_header($header);

		$qid = FILTER_VAR($this->input->post('question_id'), FILTER_SANITIZE_NUMBER_INT); 
		$aid = FILTER_VAR($this->input->post('answer_id'), FILTER_SANITIZE_NUMBER_INT);
		$uid = FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);


		$this->hasAnswered($uid, $aid);

		if( $this->form_validation->run('answer_validate') === FALSE ) {
			$this->response->json([
				'message' => 'error',
				'errors' => $this->form_validation->errors()
			]);
		}

		if( !$this->answer->isCorrect($qid, $aid) ) {
			
			if( !$explanation = $this->answer->getExplanation($qid)) {
				$this->response->json(['message' => 'there was an error']);
			}

			$this->response->json([
				'message' => 'error', 
				'explanation' => $explanation->explanation
			]);

		}

		$this->answer->addUserAnswer([
			'user_id'     => $uid,
			'answer_id'   => $aid,
			'question_id' => $qid
		]);	

		$this->response->json([
			'message' => 'correct'
		]);

	}

	private function hasAnswered($uid, $aid)
	{
		if( $this->answer->isAnswered($uid, $aid) ) {
			$this->response->json([
				'message' => 'this question has already been answered'
			]);
		}
	}


}
