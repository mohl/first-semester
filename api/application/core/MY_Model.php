<?php

class MY_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMediaId(string $table, int $id)
    {
        return $this->db
            ->select('media_id')
            ->where('id', $id)
            ->get($table)
            ->row();
    }

}
