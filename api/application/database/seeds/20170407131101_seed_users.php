<?php

class Seed_users extends Seeder
{

	/**
	 * The table to seed
	 * 
	 * @var string $table
	 */
	private $table = 'users';

	/**
	 * Data to insert
	 * 
	 * @var array $data
	 */
	private $data = [];

	/**
	 * Faker library
	 * 
	 * @var object $faker
	 */
	private $faker;

	/**
	 * The amount of rows to insert
	 * 
	 * @var int $limit
	 */
	protected $limit = 25;

	public function __construct()
	{
		parent::__construct();
		$this->faker = Faker\Factory::create();
	}

	/**
	 * Seed the table
	 * 
	 * @return void
	 */
	public function seed()
	{

		//$this->ci->db->truncate($this->table);

		// Just some dummy stuff
		for($i=0; $i < $this->limit; $i++)
		{
  			$this->data[] = [
  				'name' => $this->faker->unique()->name,
  				'email' => $this->faker->unique()->email,
  				'password' => password_hash('123', PASSWORD_DEFAULT)
  			];
		}

		$this->ci->db->insert_batch($this->table, $this->data);

	}

}