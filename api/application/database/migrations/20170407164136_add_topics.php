<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_topics extends CI_Migration {

    public function up()
    {

        echo 'Making topics.. ' . PHP_EOL;

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 65,
            ],
            'slug' => [
                'type' => 'VARCHAR',
                'constraint' => 65,
                'unique' => TRUE
            ],
            'description' => [
                'type' => 'TEXT'
            ]
        ]);

        $this->dbforge->add_field('created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('topics');
    }

    public function down()
    {
        $this->dbforge->drop_table('topics');
    }
}