<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_quizzes extends CI_Migration {

    public function up()
    {

        echo 'Making quizzes.. ' . PHP_EOL;

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 65,
            ],
            'slug' => [
                'type' => 'VARCHAR',
                'constraint' => 65,
                'unique' => TRUE
            ],
            'description' => [
                'type' => 'TEXT'
            ],
            'experience' => [
                'type' => 'INT',
                'constraint' => 5
            ],
            'sticky' => [
                'type' => 'BOOLEAN',
                'default' => 0
            ],
            'media_id' => [
                'type' => 'INT',
                'constraint' => 5
            ],
            'topic_id' => [
                'type' => 'INT',
                'constraint' => 5
            ]
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('media_id');
        $this->dbforge->add_key('topic_id');

        $this->dbforge->add_field('created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('published_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->dbforge->add_field('FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('FOREIGN KEY (topic_id) REFERENCES topics(id)');
        $this->dbforge->create_table('quizzes');
    }

    public function down()
    {
        $this->dbforge->drop_table('quizzes');
    }
}