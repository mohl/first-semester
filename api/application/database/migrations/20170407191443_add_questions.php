<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_questions extends CI_Migration {

    public function up()
    {

        echo 'Making questions.. ' . PHP_EOL;

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ],
            'title' => [
                'type' => 'TEXT',
            ],
            'explanation' => [
                'type' => 'TEXT'
            ],
            'position' => [
                'type' => 'INT',
                'constraint' => 5,
            ],
            'type' => [
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0
            ],
            'quiz_id' => [
                'type' => 'INT',
                'constraint' => 5
            ],
            'media_id' => [
                'type' => 'INT',
                'constraint' => 5
            ]

        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('quiz_id');
        $this->dbforge->add_key('media_id');

        $this->dbforge->add_field('created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->dbforge->add_field('FOREIGN KEY (quiz_id) REFERENCES quizzes(id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('questions');
    }

    public function down()
    {
        $this->dbforge->drop_table('questions');
    }
}