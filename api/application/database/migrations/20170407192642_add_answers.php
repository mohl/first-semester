<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_answers extends CI_Migration {

    public function up()
    {

        echo 'Making answers.. ' . PHP_EOL;

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ],
            'answer' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'position' => [
                'type' => 'INT',
                'constraint' => 5,
            ],
            'correct' => [
                'type' => 'BOOLEAN',
                'default' => 0
            ],
            'type' => [
                'type' => 'TINYINT',
                'default' => 0
            ],
            'question_id' => [
                'type' => 'INT',
                'constraint' => 5
            ],
            'media_id' => [
                'type' => 'int',
                'constraint' => 5
            ],
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('question_id');
        $this->dbforge->add_key('media_id');

        $this->dbforge->add_field('created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->dbforge->add_field('FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('answers');
    }

    public function down()
    {
        $this->dbforge->drop_table('answers');
    }
}