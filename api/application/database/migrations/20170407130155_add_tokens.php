<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tokens extends CI_Migration
{

    public function up()
    {

        echo 'Making tokens..' . PHP_EOL;

        $this->dbforge->add_field(array(
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ],
            'token' => [
                'type' => 'VARCHAR',
                'constraint' => 80,
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 5
            ]
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('user_id');

        $this->dbforge->add_field('created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('expires_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->dbforge->create_table('tokens');

        $this->dbforge->add_field('FOREIGN KEY (user_id) REFERENCES users(id)');
    }

    public function down()
    {
        $this->dbforge->drop_table('tokens');
    }
}