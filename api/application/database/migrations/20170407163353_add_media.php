<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_media extends CI_Migration {

    public function up()
    {

        echo 'Making media.. ' . PHP_EOL;

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ],
            'media_type' => [
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ],
            'media_url' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'media_start' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => NULL,
                'null' => TRUE
            ],
            'media_end' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => NULL,
                'null' => TRUE
            ]
        ]);

        $this->dbforge->add_field('created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('media');
    }

    public function down()
    {
        $this->dbforge->drop_table('media');
    }
}