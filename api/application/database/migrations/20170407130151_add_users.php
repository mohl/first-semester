<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_users extends CI_Migration {

    public function up()
    {

        echo 'Making users.. ' . PHP_EOL;

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                //'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 60,
                'null' => FALSE
            ],
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' => TRUE,
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 60
            ),
            'role' => [
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ]
        ));

        $this->dbforge->add_key('id', TRUE);

        $this->dbforge->add_field('created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dbforge->add_field('updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->dbforge->create_table('users');
    }

    public function down()
    {
        $this->dbforge->drop_table('users');
    }
}