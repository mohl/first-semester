<?php

class Guard
{

	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function access($role = false)
	{
		$this->ci->load->helper('filter_header');
		$this->ci->load->helper('auth_header');
		$this->ci->load->model('user');
		$this->ci->load->model('token');

		$header = auth_header();
		list($id, $tk) = filter_header($header);

		$id = FILTER_VAR($id, FILTER_SANITIZE_NUMBER_INT);
		$tk = FILTER_VAR($tk, FILTER_SANITIZE_STRING);

		if ( !$this->ci->token->whereUserIdAndTokenIs($id, $tk) OR
			 isset($role) AND !$this->ci->user->whereRoleIs($role))
		{
			$this->ci->response->status(401)->json([
				'message' => 'unauthorized'
			]);
		}
		
	}

}	