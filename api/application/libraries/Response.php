<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CodeIgniter
 * @subpackage Response
 * @author Morten Hartvig Larsen <mohl@protonmail.com>
 * @access public
 * 
 */


class Response 
{

	/**
	 * CodeIgniter instance
	 * 
	 * @var object
	 * @access private
	 */
	private $ci;

	/**
	 * HTTP status code
	 * 
	 * @var int
	 * @access private
	 */
	private $status = 200;

	public function __construct()
	{
		$this->ci = & get_instance();
	}

	/**
	 * Set the HTTP 
	 * 
	 * @param  int     $status 
	 * @return object  $this Response
	 * @access public
	 */
	public function status(int $status)
	{
		$this->status = $status;

		return $this;
	}

	/**
	 * Default response used in controllers
	 * 
	 * @param mixed
	 * @access public
	 * @return void
	 */
	public function json($output)
	{
		$this->build('application/json', json_encode($output));
	}

	/**
	 * Quick and dirty way to kill the app
	 * eg. $this->response->exit(400) (Bad request)
	 * 
	 * @param int $status HTTP status
	 * @access public
	 * @return response
	 */
	public function exit(int $status)
	{
		$this->ci->output->set_status_header($status)
			->_display();

		exit();
	}

	/**
	 * Build the response output
	 * 
	 * @param string $type   Output type
	 * @param mixed  $output Output
	 * @access private
	 * @return response
	 */
	private function build(string $type, $output)
	{
		$this->ci->output->set_content_type($type)
			->set_status_header($this->status)
			->set_output($output)
			->_display();

		exit();
	}
}