<?php

class Seeder
{

	/**
	 * Seed directory
	 *
	 * @var string $seed_path
	 */
	private $seed_directory = APPPATH . 'database/seeds/';

	/**
	 * Template directory
	 * 
	 * @var string $template_directory
	 */
	private $template_directory = APPPATH . 'database/templates/seed.txt';

	/**
	 * Seed enabled
	 * 
	 * @var bool $seed_enabled
	 */
	private $seed_enabled = FALSE;

	/**
	 * A collection of seeds
	 * 
	 * @var array $seeds
	 */
	private $seeds = [];

	/**
	 * Placeholder in our template
	 * 
	 * @var string $placeholder
	 */
	private $placeholder = '@seed@';

	/**
	 * CodeIginiter instance
	 * 
	 * @var object $ci
	 */
	protected $ci;

	public function __construct()
	{
		
		$this->ci =& get_instance();
		
		$this->ci->config->load('seeder');
		$this->seed_enabled       = $this->ci->config->item('seed_enabled');
		$this->seed_path          = $this->ci->config->item('seed_path');
		$this->template_directory = $this->ci->config->item('template_directory');

		if ( ! $this->seed_enabled === TRUE)
		{
			show_error('Seeder: Seeding is not enabled.');
		}

	}

	/**
	 * Get the seeds
	 *
	 * @return void
	 */
	private function get_seeds()
	{

		if( ! $this->seeds = glob($this->seed_directory . '*seed*.php'))
		{
			show_error('Seeder: No seeds could be found');
		}

	}

	/**
	 * Create a seed
	 * 
	 * @param string $seed
	 */
	public function make(string $seed)
	{

		$date = new Datetime();
		$timestamp = $date->format('YmdHis');

		$file = $this->seed_directory . $timestamp . '_seed_' . strtolower($seed) . '.php';

		copy($this->template_directory, $file) 
		OR show_error('Seeder: Unable to copy template');

		if ( !$str = file_get_contents($file) OR
			 !$str = str_replace($this->placeholder, $seed, $str) OR
			 !file_put_contents($file, $str))
		{
			show_error('Seeder: Unable to replace placeholder in seed "' . $seed . '"');
		}

	}

	/**
	 * Seed the database
	 * 
	 * @return int
	 */
	public function run()
	{	

		$this->get_seeds();

		foreach ($this->seeds as $seed)
		{
			require_once $seed;

			$seed = ltrim($seed, $this->seed_directory);
			$seed = preg_replace('/\d{14}\w{1}/', '', $seed);
			$class = ucfirst(trim($seed, '.php'));

			#$class = rtrim(ucfirst(preg_replace('%([a-z]+)+(/\d+_)%', '', $seed)), '.php');

			#$class = rtrim(ucfirst($seed));

			$class = new $class;
			$class->seed();

			unset($class);
		}

		return count($this->seeds);

	}

}