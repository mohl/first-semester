<?php

/**
 * Extend CI_Migration
 */

class MY_Migration extends CI_Migration
{

	/**
	 * Migration placeholder
	 * Used in the migration template
	 * 
	 * @var string $placeholder
	 */
	private $placeholder = '@@migration@@';

	/**
	 * Make a migration
	 * 
	 * @param string $migration
	 * @return bool
	 */
	public function make(string $migration)
	{

    	$time = new DateTime();
    	$timestamp = $time->format('YmdHis');

		$file = $this->_migration_path . $timestamp . '_add_' . $migration .'.php';
		
		copy(APPPATH . 'templates/migration.txt', $file)
		OR show_error('Unable to copy migration template');

		$str = file_get_contents($file);
		$str = str_replace($this->placeholder, $migration, $str);
		file_put_contents($file, $str);

		return true;
	}

}