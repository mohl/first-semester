<?php

class MY_Form_validation extends CI_Form_validation
{

    protected $ci;

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->ci =& get_instance();
        $this->ci->load->model('question');
    }

    public function errors()
    {
        if (count($this->_error_array) === 0)
        {
            return FALSE;
        }
        else 
        {
            return $this->_error_array;
        }
    }

    public function validate_unique($str, $field) 
    {
        list($table, $column) = explode('.', $field);
        $string = FILTER_VAR($str, FILTER_SANITIZE_STRING);

        // Check when creating a new resource
        if ( $this->ci->input->server('REQUEST_METHOD') == 'POST' ) {
            if ( $this->ci->db->select($column)->where($column, $string)->get($table)->row() ) {
                $this->set_message('validate_unique', 'The {field} already exist.');
                return false;
            }
        }

        // When updating
        if ( $this->ci->input->server('REQUEST_METHOD') == 'PUT' ) {
      
            $id = (int) FILTER_VAR($this->ci->input->input_stream('id'), FILTER_SANITIZE_NUMBER_INT);

            if ( !$result = $this->ci->db->select('id, ' . $column)->where($column, $string)->get($table)->row() ) {
                return true;
            }

            if ( $result->id == $this->ci->input->input_stream('id') && 
                 $result->{$column} == $this->ci->input->input_stream($column) ) {
                return true;
            }

            $this->set_message('validate_unique', 'The {field} already exist.');
            return false;

        }
    }

    public function validate_question_id($id)
    {
        $this->ci->load->model('question');

       if ( !$test = $this->ci->question->validateByID($id)) {
            $this->set_message('validate_question_id', 'The {field} does not exist.');
            return false;
        }
    }

    public function validate_topic_id($id)
    {
        $this->ci->load->model('topic');

        if ( !$test = $this->ci->topic->validateByID($id)) {
            $this->set_message('validate_topic_id', 'The {field} does not exist.');
            return false;
        }
    }

    public function validate_quiz_id($id)
    {
        $this->ci->load->model('quiz');

        if ( !$test = $this->ci->quiz->validateByID($id)) {
            $this->set_message('validate_quiz_id', 'The {field} does not exist!');
            return false;
        }
    }

    public function validate_answer_id($id)
    {
        $this->ci->load->model('answer');

        if ( !$test = $this->ci->answer->validateByID($id)) {
            $this->set_message('validate_answer_id', 'The {field} does not exist!');
            return false;
        }
    }

    public function valid_url($url)
    {   
        $pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
        if (!preg_match($pattern, $url)) {
            return FALSE;
        }

        return TRUE;
    }

}
