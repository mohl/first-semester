import Vue from 'vue'
import Router from 'vue-router'

// Components
import BackEnd from '@/components/admin/BackEnd'
import Classes from '@/components/admin/Classes'
import EditQuiz from '@/components/admin/quizzes/Edit'
import FrontPage from '@/components/public/FrontPage'
import Quiz from '@/components/public/Quiz'
import Quizzes from '@/components/admin/quizzes/Quizzes'
import Students from '@/components/admin/Students'
import Topics from '@/components/admin/Topics'

Vue.use(Router)

const routes = [
    { path: '/', name: 'front-page', component: FrontPage },
    { path: '/admin', name: 'back-end', component: BackEnd,
        children: [
            { path: 'quizzes', name: 'quizzes', component: Quizzes, },
            { path: 'quizzes/edit', name: 'edit-quiz', component: EditQuiz },
            { path: 'topics', name: 'topics', component: Topics },
            { path: 'classes', name: 'classes', component: Classes },
            { path: 'students', name: 'students', component: Students }
        ]
    },
    { path: '/quiz', name: 'quiz', component: Quiz }
]

const router = new Router({ routes })

export default router