

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import FrontEnd from '@/components/public/FrontEnd'
import router from './router'
import axios from 'axios'
import VueYouTubeEmbed from 'vue-youtube-embed'
import moment from 'moment'

Vue.use(VueYouTubeEmbed)

axios.defaults.headers.common = {
	'Authorization': 'Basic ' + btoa(localStorage.getItem('id') + ':' + localStorage.getItem('api_token'))
};

export const api = axios.create({
	baseURL: 'http://localhost/first-semester/api/index.php/'
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
	router,
	template: '<FrontEnd/>',
	el: '#app',
	components: { FrontEnd }
})

